Textual robots
==============

.. inheritance-diagram:: trobots

.. automodule:: trobots
    :members:
