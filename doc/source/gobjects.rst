Graphical objects
=================

.. inheritance-diagram:: gobjects

.. automodule:: gobjects
    :members:
