Formatter helper
================

.. inheritance-diagram:: hformatter

.. automodule:: hformatter
    :members:
