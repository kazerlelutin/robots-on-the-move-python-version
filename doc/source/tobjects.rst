Textual objects
===============

.. inheritance-diagram:: tobjects

.. automodule:: tobjects
    :members:
