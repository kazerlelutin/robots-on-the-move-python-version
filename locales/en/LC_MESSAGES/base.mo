��    9      �              �  	   �     �     �     �     �  !   �               $     )     5     D     V     e     g     j     o     u     }     �     �     �     �     �     �  8   �  9        >     Q     S      Y     z     �     �     �     �  7   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �            T   
  E   _  +   �  $  �  	   �                !     8  !   D     f     h     m     r     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �  8   	  9   M	     �	     �	     �	      �	     �	     �	     �	     �	     �	  7   �	     
     
     
     
     !
     &
     (
     +
     0
     6
     >
     @
     F
     H
     M
     O
  T   S
  E   �
  +   �
   
Running: Anonymous object Anonymous robot Continue (yes or no)?  Crazy robot Display program version and exit. E EAST East Fixed robot Initial state: Interactive robot Invalid input. N NO NONE NORTH NOTHING No None North Nothing Obstinate robot Optional arguments Positional arguments Program making several robots move in a graphical world. Program making several robots move in a minimalist world. Robots on the move S SOUTH Show this help message and exit. South Usage:  W WEST West Which direction: north, south, east, west, or nothing?  Y YES Yes e east n no none north nothing s south w west y yes {current_object.object_name} is moving from {current_object.position} to {position}. {current_object.object_name} is staying at {current_object.position}. {robot.object_name} is at {robot.position}. Project-Id-Version: 1.0.0
POT-Creation-Date: 2023-05-03 22:11+0200
PO-Revision-Date: 2023-05-03 22:11+0200
Last-Translator: Le Bars, Yoann
Language-Team: Le Bars, Yoann
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 
Running: Anonymous object Anonymous robot Continue (yes or no)?  Crazy robot Display program version and exit. E EAST East Fixed robot Initial state: Interactive robot Invalid input. N NO NONE NORTH NOTHING No None North Nothing Obstinate robot Optional arguments Positional arguments Program making several robots move in a graphical world. Program making several robots move in a minimalist world. Robots on the move S SOUTH Show this help message and exit. South Usage:  W WEST West Which direction: north, south, east, west, or nothing?  Y YES Yes e east n no none north nothing s south w west y yes {current_object.object_name} is moving from {current_object.position} to {position}. {current_object.object_name} is staying at {current_object.position}. {robot.object_name} is at {robot.position}. 