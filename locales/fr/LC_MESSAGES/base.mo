��    9      �              �  	   �     �     �     �     �  !   �               $     )     5     D     V     e     g     j     o     u     }     �     �     �     �     �     �  8   �  9        >     Q     S      Y     z     �     �     �     �  7   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �            T   
  E   _  +   �  $  �     �                  	   <  *   F     q     s     w  
   {     �     �     �     �     �     �     �     �     �     �     �     �     �     �     		  B    	  D   c	     �	     �	     �	  (   �	     �	  	   �	     �	     �	      
  8   
     ?
     A
     E
     I
     K
     O
     Q
     U
     \
     a
     f
     h
     l
     n
     t
     v
  V   z
  @   �
  3      
Running: Anonymous object Anonymous robot Continue (yes or no)?  Crazy robot Display program version and exit. E EAST East Fixed robot Initial state: Interactive robot Invalid input. N NO NONE NORTH NOTHING No None North Nothing Obstinate robot Optional arguments Positional arguments Program making several robots move in a graphical world. Program making several robots move in a minimalist world. Robots on the move S SOUTH Show this help message and exit. South Usage:  W WEST West Which direction: north, south, east, west, or nothing?  Y YES Yes e east n no none north nothing s south w west y yes {current_object.object_name} is moving from {current_object.position} to {position}. {current_object.object_name} is staying at {current_object.position}. {robot.object_name} is at {robot.position}. Project-Id-Version: 1.0.0
POT-Creation-Date: 2023-05-03 22:11+0200
PO-Revision-Date: 2023-05-03 22:11+0200
Last-Translator: Le Bars, Yoann
Language-Team: Le Bars, Yoann
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 
Execution : Objet anonyme Robot anonyme Continuer (oui ou non) ?  Robot fou Affiche la version du programme et quitte. E EST Est Robot fixe État initial : Robot interactif Saisie invalide. N NON AUCUNE NORD RIEN Non Aucune Nord Rien Robot obstiné Arguments optionnels Arguments positionnels Programme faisant s’ébaudir des robots dans un monde graphique. Programme faisant s’ébaudir des robots dans un monde minimaliste. Robots en goguette S SUD Affiche ce message d’erreur et quitte. Sud Usage :  O OUEST Ouest Quelle direction : nord, sud, est, ouest ou aucune ?  O OUI Oui e est n non aucune nord rien s sud o ouest o oui {current_object.object_name} se déplace de {current_object.position} vers {position}. {current_object.object_name} reste en {current_object.position}. {robot.object_name} est situé en {robot.position}. 