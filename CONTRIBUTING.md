Contributing to this Project
============================

Every kind of help is welcome!

As a starter, you probably should read the
[project wiki](https://framagit.org/ylebars/robots-on-the-move-python-version/-/wikis/home "Project wiki").

Do not hesitate to contact Yoann Le Bars (you can find his coordinates in the
file [`AUTHORS`](AUTHORS "AUTHORS")).

**Contents**

- [Process](#process "Process")
- [Coding conventions](#coding-conventions "Coding conventions")

Process
-------

Anyone can submit some change. You can submit patches as well as merging
requests. We prefer you submit several small commits rather than huge ones:
conflicts are more easily solved with small submissions.

Coding conventions
------------------

* Indentation should use four spaces and no tabulation;
* you should remove white-spaces at the end of lines;
* naming conventions:
    1. names for classes should start in upper-case, while names for objects,
    variables and function should start in lower-case;
    2. in names composed with several words, each word should be separate with
    upper-case in classes names, with underscore (“_”) in variables and
    functions names;
    3. names for constant should entirely be in upper case;
    4. protected object names are prefixed with one underscore, private
    object names with two underscores;
* comments guidelines:
    * comments should comply to
    [Sphinx](https://www.sphinx-doc.org/en/master/ "Sphinx documentation system")
    format;
    * for every method, function, and procedure, a `:param:` field should be
    set for every parameter;
    * for every method and function, the field `:returns:` should be filled
    in;
    * every declared variable should be documented with a comment;
    * the field `:date:` should not be used, as Git does keep a precise track
    of modifications and when they happened;
    * as well, the field `:version:` should not be used, as it does not give
    more information than Git does, and Git is more accurate.
