#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions for graphical world.

:module: gworld
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import Any, AnyStr, Final
from multimethod import multimethod
import pygame
from translator import _
from robots import Position
from world import World
from grobots import GraphicalRobot
from gobjects import SPRITESIZE


class GraphicalWorld(World[GraphicalRobot]):
    """
    Defining a graphical world.

    :param bool __running: Whether the program should continue running.
    :param pygame.Surface __screen: The surface where drawing.
    :param pygame.Surface __background: Background descriptor.
    :param Final[int] __HALF_SIZE: Sprite half size.
    """

    __running: bool
    __surface: pygame.Surface
    __background: pygame.Surface
    __HALFSIZE: Final[int]

    def __init__(self, background_file: AnyStr = '', length: int = 0,
                 height: int = 0) -> None:
        """
        Class initialization.

        :param int length: World length. Defaults to 0.
        :param int height: World height. Defaults to 0.
        """

        World.__init__(self, length, height)
        self.__running = True
        # Half sprite size.
        self.__HALFSIZE = SPRITESIZE // 2    # pylint: disable=invalid-name

        pygame.init()
        self.__screen = pygame.display.set_mode((640, 455))
        pygame.display.set_caption(_('Robots on the move'))
        self.__background = pygame.image.load(background_file).convert()
        # Group of all robots sprites.
        self.__sprite_group = pygame.sprite.RenderPlain()

    def add_object(self, new_object: GraphicalRobot) -> None:
        """
        Add a robot in the world.

        :param GraphicalRobot new_object: The robot to be added.
        """

        self._objects.append(new_object)
        self.__sprite_group.add(new_object)

    @multimethod
    def is_free(self, position: Position,    # pylint: disable=arguments-differ
                current_object: GraphicalRobot) -> bool:
        """
        Indicates if a position is available.

        :param Position position: Position to be tested.
        :param GraphicalRobot current_object: Robot to test.

        :returns: Whether the position is available.
        :rtype: bool
        """

        # List of objects to be tested.
        others: list[GraphicalRobot] = self.objects.copy()
        others.remove(current_object)
        # Rectangle list.
        rectangles_list: list[Any] = [current.rect for current in others]
        # Rectangle for the new coordinates.
        RECTANGLE: Final[Any] = pygame.Rect(position[0] - self.__HALFSIZE,    # pylint: disable=invalid-name
                                            position[1] - self.__HALFSIZE,
                                            SPRITESIZE, SPRITESIZE)

        return RECTANGLE.collidelist(rectangles_list) == -1

    def is_legal(self, position: Position) -> bool:
        """
        Indicates if a position is valid in the given world.

        :param Position position: Position to be tested.

        :returns: Whether the position is available.
        :rtype: bool
        """

        return self.__HALFSIZE <= position[0] < self.length - self.__HALFSIZE\
            and self.__HALFSIZE <= position[1] < self.height - self.__HALFSIZE

    @multimethod
    def _move_object(self, current_object: GraphicalRobot,    # pylint: disable=arguments-differ
                     event: pygame.event.EventType) -> None:
        """
        Moves a given object.

        :param GraphicalRobot current_object: The robot to be moved.
        :param pygame.event.EventType event: Event to be processed.
        """

        # Asked next position.
        position: Position = current_object.next_position(event)
        if self.is_legal(position) and self.is_free(position, current_object):
            current_object.position = position
            current_object.change_position(position[0], position[1])

    def _determine_continue_running(self) -> bool:
        """
        Determine if the program should continue running.

        :returns: Whether continue.
        :rtype: bool
        """

        return self.__running

    def run(self) -> None:
        """
        Program main loop.
        """

        while self.__running:
            # Current event.
            event: pygame.EventType
            for event in pygame.event.get():
                match event.type:
                    case pygame.QUIT:
                        self.__running = False
                    case pygame.KEYDOWN:
                        if event.key in (pygame.K_q, pygame.K_ESCAPE):
                            self.__running = False

            for current_object in self._objects:
                self._move_object(current_object, event)    # pylint: disable=too-many-function-args

            self.__screen.blit(self.__background, (0, 0))
            self.__sprite_group.draw(self.__screen)
            pygame.display.update()

        pygame.quit()
