#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions of graphical robots.

:module: grobots
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


import abc
from typing import AnyStr
import pygame
from multimethod import multimethod
from robots import Position, Robot, FixedRobot, CrazyRobot, ObstinateRobot,\
    InteractiveRobot, Direction
from gobjects import GraphicalObject


class GraphicalRobot(Robot, GraphicalObject, metaclass=abc.ABCMeta):
    """
    Parent class to define robots.
    """

    def __init__(self, position: Position, file_path: AnyStr) -> None:
        """
        Class initialization.

        :param Position position: Robot current coordinates.
        :param AnyStr file_path: Path to the associated sprite.
        """

        Robot.__init__(self, position)
        GraphicalObject.__init__(self, file_path)
        self._rect.center = position

    @abc.abstractmethod
    @multimethod
    def next_position(self, event: pygame.event.EventType) -> Position:    # pylint: disable=function-redefined, arguments-differ
        """
        Determine in which position the robot will go.

        :param pygame.event.EventType event: Event to proceed.

        :returns: Wanted position coordinates.
        :rtype: Position
        """


class FixedGraphicalRobot(FixedRobot, GraphicalRobot):
    """
    Not moving textual robot.
    """

    def __init__(self, position: Position, file_path: AnyStr) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
        :param AnyStr file_path: Path to the associated sprite.
        """

        FixedRobot.__init__(self, position)
        GraphicalRobot.__init__(self, position, file_path)

    def next_position(self, event: pygame.event.EventType) -> Position:    # pylint: disable=arguments-differ, unused-argument
        """
        Determine in which position the robot will go.

        :param pygame.event.EventType event: Event to process.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        return FixedRobot.next_position(self)


class CrazyGraphicalRobot(CrazyRobot, GraphicalRobot):
    """
    Class defining a robot which moves randomly.
    """

    def __init__(self, position: Position, file_path: AnyStr) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
        :param AnyStr file_path: Path to the associated sprite.
        """

        CrazyRobot.__init__(self, position)
        GraphicalRobot.__init__(self, position, file_path)

    def next_position(self, event: pygame.event.EventType) -> Position:    # pylint: disable=arguments-differ, unused-argument
        """
        Determine in which position the robot will go.

        :param pygame.event.EventType event: Event to process.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        return CrazyRobot.next_position(self)


class ObstinateGraphicalRobot(ObstinateRobot, GraphicalRobot):
    """
    Class defining a robot which always moves in the same direction as long
    as it is possible.
    """

    def __init__(self, position: Position, file_path: AnyStr) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
        :param AnyStr file_path: Path to the associated sprite.
        """

        ObstinateRobot.__init__(self, position)
        GraphicalRobot.__init__(self, position, file_path)

    def next_position(self, event: pygame.event.EventType) -> Position:    # pylint: disable=arguments-differ, unused-argument
        """
        Determine in which position the robot will go.

        :param pygame.event.EventType event: Event to process.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        return ObstinateRobot.next_position(self)


class InteractiveGraphicalRobot (InteractiveRobot, GraphicalRobot):
    """
    Defines a robot which receive moving orders from the outside.

    :param Direction __direction: Robot direction.
    """

    __direction: Direction

    def __init__(self, position: Position, file_path: AnyStr) -> None:
        InteractiveRobot.__init__(self, position)
        GraphicalRobot.__init__(self, position, file_path)
        self.__direction = Direction.NOTHING

    def _get_move(self) -> Direction:
        """
        Method allowing the robot to receive some moving order.

        :returns: The direction in which the robot should go.
        :rtype: Direction
        """

        return Direction.NOTHING

    @multimethod
    def next_position(self, event: pygame.event.EventType) -> Position:    # pylint: disable=arguments-differ, function-redefined
        """
        Determine in which position the robot will go.

        :param pygame.event.EventType event: Event to proceed.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        match event.type:
            case pygame.KEYDOWN:
                match event.key:
                    case pygame.K_LEFT:
                        self.__direction = Direction.WEST
                    case pygame.K_RIGHT:
                        self.__direction = Direction.EAST
                    case pygame.K_UP:
                        self.__direction = Direction.SOUTH
                    case pygame.K_DOWN:
                        self.__direction = Direction.NORTH
            case pygame.KEYUP:
                if event.key in (pygame.K_LEFT, pygame.K_RIGHT, pygame.K_UP,
                                 pygame.K_DOWN):
                    self.__direction = Direction.NOTHING

        return self._walk(self.__direction)
