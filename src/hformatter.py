#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Utility class to format help text.

:module: trobots
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import Iterable, Final
import argparse
from constants import __version__
from translator import _


#: Name designating positional arguments.
POSITIONALNAME: Final[str] = _('Positional arguments')
#: Name designating optional arguments.
OPTIONALNAME: Final[str] = _('Optional arguments')
#: Version action help message.
VERSIONMESSAGE: Final[str] = _('Display program version and exit.')
#: Help action help message.
HELPMESSAGE: Final[str] = _('Show this help message and exit.')


class HelpFormatter(argparse.HelpFormatter):
    """
    Class to format help output.
    """

    def add_usage(self, usage: str, actions: Iterable[argparse.Action],
                  groups: Iterable[argparse._ArgumentGroup],    # pylint: disable=protected-access
                  prefix: str = _('Usage: ')) -> None:
        """
        Reformat usage message.

        :param str usage: Program command line description.
        :param str actions: Action identifier.
        :param str groups: Groups identifier.
        :param str prefix: Prefix to usage explanation. Defaults to “Usage: ”.

        :returns: Object describing usage string.
        :rtype: None
        """

        return super().add_usage(usage, actions, groups, prefix)


def create_parser(program_description: str) -> argparse.ArgumentParser:
    """
    Generates an argument parser.

    :param str program_description: String describing the aims of the program.

    :returns: An argument parser.
    :rtype: argparse.ArgumentParser
    """

    # Command line parser.
    parser = argparse.ArgumentParser(add_help=False,
                                     formatter_class=HelpFormatter,
                                     description=program_description)
    parser._positionals.title = POSITIONALNAME    # pylint: disable=protected-access
    parser._optionals.title = OPTIONALNAME        # pylint: disable=protected-access
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s ' + __version__,
                        help=VERSIONMESSAGE)
    parser.add_argument('-h', '--help', action='help',
                        default=argparse.SUPPRESS, help=HELPMESSAGE)

    return parser
