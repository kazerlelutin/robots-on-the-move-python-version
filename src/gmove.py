#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Making robots move in a graphical world.

:module: gmove
:author: Le Bars, Yoann

:returns: 0 if everything went fine, 1 if given an invalid direction.
:rtype: int

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import Final, AnyStr
from constants import __version__
from errors import InvalidDirection
from translator import _
from hformatter import create_parser
from gworld import GraphicalWorld
from grobots import FixedGraphicalRobot, CrazyGraphicalRobot,\
    ObstinateGraphicalRobot, InteractiveGraphicalRobot


# Main part of the script.
if __name__ == '__main__':
    import sys
    import os

    try:
        # Command line parser.
        parser = create_parser(_('Program making several robots move in a '
                                 'graphical world.'))
        # Command line arguments.
        args = parser.parse_args()

        # Directory in which is current script.
        CURRDIR: Final[AnyStr] = os.path.dirname(os.path.realpath(__file__))
        # Path to resources.
        RESOURCESPATH: Final[AnyStr] = os.path.join(CURRDIR, '../resources/')
        # Path to the background image.
        BACKGROUNDPATH: Final[AnyStr] = os.path.join(RESOURCESPATH,
                                                     'background.jpg')
        # Path to fixed robot image.
        FIXEDPATH: Final[AnyStr] = os.path.join(RESOURCESPATH, 'stone.jpg')
        # Path to crazy robot image.
        CRAZYPATH: Final[AnyStr] = os.path.join(RESOURCESPATH, 'bug.png')
        # Path to obstinate robot image.
        OBSTINATEPATH: Final[AnyStr] = os.path.join(RESOURCESPATH,
                                                    'obstinate.png')
        # Path to interactive robot image.
        INTERACTIVEPATH: Final[AnyStr] = os.path.join(RESOURCESPATH,
                                                      'robot-face.png')

        # The world to be populated.
        world: GraphicalWorld = GraphicalWorld(BACKGROUNDPATH, 640, 455)
        # First robot in the world: a fixed robot.
        robot1: FixedGraphicalRobot = FixedGraphicalRobot((150, 200), FIXEDPATH)
        # Second robot in the world: a crazy robot.
        robot2: CrazyGraphicalRobot = CrazyGraphicalRobot((320, 200), CRAZYPATH)
        # Third robot in the world: an obstinate robot.
        robot3: ObstinateGraphicalRobot = ObstinateGraphicalRobot((400, 50),
                                                                  OBSTINATEPATH)
        # Fourth robot in the world: an interactive robot.
        robot4: InteractiveGraphicalRobot =\
            InteractiveGraphicalRobot((35, 40), INTERACTIVEPATH)
        world.add_object(robot1)
        world.add_object(robot2)
        world.add_object(robot3)
        world.add_object(robot4)
        world.run()
    except InvalidDirection:
        print('Error 1: invalid given direction.', file=sys.stderr)
        sys.exit(1)
