#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions of textual robots.

:module: trobots
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


import abc
from robots import Position, Robot, FixedRobot, CrazyRobot, ObstinateRobot,\
    InteractiveRobot, Direction
from tobjects import TextualObject
from translator import _


# List of responses to  indicate northern direction.
direction_north: list[str] = [_('n'), _('N'), _('north'), _('North'),
                              _('NORTH')]
# List of responses to  indicate southern direction.
direction_south: list[str] = [_('s'), _('S'), _('south'), _('South'),
                              _('SOUTH')]
# List of responses to  indicate eastern direction.
direction_east: list[str] = [_('e'), _('E'), _('east'), _('East'), _('EAST')]
# List of responses to  indicate western direction.
direction_west: list[str] = [_('w'), _('W'), _('west'), _('West'), _('WEST')]
# List of responses to  indicate no direction.
direction_none: list[str] = [_('none'), _('None'), _('NONE'), _('nothing'),
                             _('Nothing'), _('NOTHING')]


class TextualRobot (Robot, TextualObject, metaclass=abc.ABCMeta):    # pylint: disable=too-few-public-methods
    """
    Robots type for textual print.
    """

    def __init__(self, position: Position = (0, 0),
                 robot_name: str = _('Anonymous robot')) -> None:
        """
        Class initialization.

        :param Position position: Coordinates for initial position of the
                                  robot. Defaults to (0, 0).
        :param str robot_name: Robot identifying name. Defaults to
                               “Anonymous robot”.
        """

        Robot.__init__(self, position)
        TextualObject.__init__(self, robot_name)


class FixedTextualRobot(FixedRobot, TextualRobot):    # pylint: disable=too-few-public-methods
    """
    Not moving textual robot.
    """

    def __init__(self, position: Position = (0, 0),
                 robot_name: str = _('Fixed robot')) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
                                  Defaults to (0, 0).
        :param str robot_name: Robot identifying name. Defaults to
                               “Fixed robot”.
        """

        FixedRobot.__init__(self, position)
        TextualRobot.__init__(self, position, robot_name)


class CrazyTextualRobot(CrazyRobot, TextualRobot):    # pylint: disable=too-few-public-methods
    """
    Randomly moving robot.
    """

    def __init__(self, position: Position = (0, 0),
                 robot_name: str = _('Crazy robot')) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
                                  Defaults to (0, 0).
        :param str robot_name: Robot identifying name. Defaults to
                               “Crazy robot”.
        """

        CrazyRobot.__init__(self, position)
        TextualRobot.__init__(self, position, robot_name)


class ObstinateTextualRobot(ObstinateRobot, TextualRobot):    # pylint: disable=too-few-public-methods
    """
    Robot moving in a given direction while it is possible.
    """

    def __init__(self, position: Position = (0, 0),
                 robot_name: str = _('Obstinate robot')) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
                                  Defaults to (0, 0).
        :param str robot_name: Robot identifying name. Defaults to
                               “Obstinate robot”.
        """

        ObstinateRobot.__init__(self, position)
        TextualRobot.__init__(self, position, robot_name)


class InteractiveTextualRobot(InteractiveRobot, TextualRobot):    # pylint: disable=too-few-public-methods
    """
    Robot receiving moving orders from the outside.
    """

    def __init__(self, position: Position = (0, 0),
                 robot_name: str = _('Interactive robot')) -> None:
        """
        Class initialization.

        :param Position position: Robot initial position coordinates.
                                  Defaults to (0, 0).
        :param str robot_name: Robot identifying name. Defaults to
                               “Interactive robot”
        """

        InteractiveRobot.__init__(self, position)
        TextualRobot.__init__(self, position, robot_name)

    def _get_move(self) -> Direction:    # pylint: disable=no-self-use
        """
        Method allowing the robot to receive some moving order.

        :returns: The direction in which the robot should go.
        :rtype: Direction
        """

        while True:
            # User input.
            response: str = input(_('Which direction: north, south, east, '
                                    'west, or nothing? '))
            if response in direction_north:
                return Direction.NORTH
            if response in direction_south:
                return Direction.SOUTH
            if response in direction_east:
                return Direction.EAST
            if response in direction_west:
                return Direction.WEST
            if response in direction_none:
                return Direction.NOTHING
            print(_('Invalid input.'))
