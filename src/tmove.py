#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Making robots move in a textual world.

:module: tmove
:author: Le Bars, Yoann

:returns: 0 if everything went fine, 1 if given an invalid direction.
:rtype: int

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from errors import InvalidDirection
from trobots import FixedTextualRobot, CrazyTextualRobot,\
    ObstinateTextualRobot, InteractiveTextualRobot
from tworld import TextualWorld
from translator import _
from hformatter import create_parser


# Main part of the script.
if __name__ == '__main__':
    import sys

    try:
        # Command line parser.
        parser = create_parser(_('Program making several robots move in a '
                                 'minimalist world.'))
        # Command line arguments.
        args = parser.parse_args()

        # The world to be populated.
        world: TextualWorld = TextualWorld(5, 5)
        # First robot in the world: a fixed robot.
        robot1: FixedTextualRobot = FixedTextualRobot.from_int(3, 3)
        # Second robot in the world: a crazy robot.
        robot2: CrazyTextualRobot = CrazyTextualRobot.from_int(2, 2)
        # Third robot in the world: an obstinate robot.
        robot3: ObstinateTextualRobot = ObstinateTextualRobot.from_int(1, 1)
        # Fourth robot in the world: an interactive robot.
        robot4: InteractiveTextualRobot = InteractiveTextualRobot.from_int(0, 0)
        world.add_object(robot1)
        world.add_object(robot2)
        world.add_object(robot3)
        world.add_object(robot4)
        world.run()
    except InvalidDirection:
        print('Error 1: invalid given direction.', file=sys.stderr)
        sys.exit(1)
