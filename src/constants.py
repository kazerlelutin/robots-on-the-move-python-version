#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
General constant definitions.

:module: constants
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""

from typing import Final

#: Version major number.
MAJOR: Final[str] = '1'
#: Version minor number.
MINOR: Final[str] = '0'
#: Maintenance version number.
MAINTENANCE: Final[str] = '0'
#: Program version.
__version__: Final[str] = MAJOR + '.' + MINOR + '.' + MAINTENANCE
